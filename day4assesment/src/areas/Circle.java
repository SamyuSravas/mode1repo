package areas;

public class Circle extends Shape{

		private final double radius;
		private double pi = Math.PI;
		@Override
		public double area() {
			return pi * Math.pow(radius,2);
			
		}
		@Override
		public double perimeter(){
			return 2*pi*radius;
		}
		
		public Circle(double radius){
			super();
			this.radius = radius;
		}
		
		
		
		
	}


