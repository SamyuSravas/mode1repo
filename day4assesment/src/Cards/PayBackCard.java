package Cards;

public class PayBackCard extends Cards {
	private int pointsEarned;
	private double totalamount;
public PayBackCard(String holderName, String cardNumber, String expiryDate) {
		super(holderName, cardNumber, expiryDate);
	}
public int getPointsEarned() {
	return pointsEarned;
}
public void setPointsEarned(int pointsEarned) {
	this.pointsEarned = pointsEarned;
}
public double getTotalamount() {
	return totalamount;
}
public void setTotalamount(double totalamount) {
	this.totalamount = totalamount;
}
public PayBackCard(String holderName, String cardNumber, String expiryDate, int pointsEarned, double totalamount) {
	super(holderName, cardNumber, expiryDate);
	this.pointsEarned = pointsEarned;
	this.totalamount = totalamount;
}



}
 
