
public class PayBackClass {
	public class Card extends PayBackClass{
		 Integer pointsEarned; 

		 Double totalAmount;

		public Integer getPointsEarned() {
			return pointsEarned;
		}

		public void setPointsEarned(Integer pointsEarned) {
			this.pointsEarned = pointsEarned;
		}

		public Double getTotalAmount() {
			return totalAmount;
		}

		public void setTotalAmount(Double totalAmount) {
			this.totalAmount = totalAmount;
		}

		public Card(Integer pointsEarned, Double totalAmount) {
			super();
			this.pointsEarned = pointsEarned;
			this.totalAmount = totalAmount;
		} 

}
}