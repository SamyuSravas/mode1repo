
public class MembershipCard {
	public class Card extends MembershipCard {
		
			
		
		private int rating;


		public int getRating() {
			return rating;
		}

		public void setRating(int rating) {
			this.rating = rating;
		}


		public Card(int rating) {
			super();
			this.rating = rating;
		}

	}
}
