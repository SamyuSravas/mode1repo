package com.hcl.model;

public class Replacement {

	private String str;
	private char[] arrChar;
	
	public Replacement(){
	}
	public Replacement(String str){
		this.str = str;
	}
	public char[] Replace() {
		arrChar = str.toCharArray();
		for(int i = 0; i < arrChar.length; i++){
			if(arrChar[i] == 'd'){
				arrChar[i] = 'n';
			}
		}
	return arrChar;
	}
}
