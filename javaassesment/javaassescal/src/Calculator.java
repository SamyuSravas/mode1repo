
public class Calculator {

	public static void main(String[] args) {
		int number1 = 20;
		int number2 = 4;
		int sum = number1 + number2;
		int difference = number1 - number2;
		int multiplication = number1 * number2;
		int division = number1 / number2;
		int remainder = number1 % number2;
		System.out.println("the sum is            : " + sum);
		System.out.println("the difference is     : " + difference);
		System.out.println("the multiplication is : " + multiplication);
		System.out.println("the division is       : " + division);
		System.out.println("the remainder is      : " + remainder);

	}

}
