import java.util.Scanner;
public class SquareEvenDigits {
	public static void main(String[] args) {
		int sumOfEven = sumOfSquaresOfEvenDigits();
		System.out.println("the sum of even digits in given number:" +sumOfEven);
	}
	public static int sumOfSquaresOfEvenDigits(){
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number:");
		int num = sc.nextInt();
		int digit;
		int sum = 0;
		while(num!=0){
			digit = num%10;
			if(digit%2 ==0){
				sum =sum+(digit*digit);
			
			}
			num = num/10;
		}
		return sum;
	}
	

}
