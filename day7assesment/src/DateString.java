import java.util.*;
import java.text.*;
public class DateString {
	public static void main(String[] args) {
		try{
			String originalString = "2021-02-24 09:00:02";
			Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(originalString);
			String newstr = new SimpleDateFormat("MM/dd/yyyy, H:mm:ss").format(date);
			System.out.println("\n"+newstr+"\n");
			
		}
		catch(ParseException e){
			e.printStackTrace();
		}
	}

}
