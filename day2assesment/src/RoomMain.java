
public class RoomMain {
	public static void main(String[] args) {

		Room room = new Room();
		
		Room.roomNo        = 5656;
		Room.roomType      = "rectangle";
		Room.roomArea      = 6776f;
		Room.roomAcmachine = 44;
		System.out.println("Room number    :"    + Room.roomNo);
		System.out.println("Room type      :"    + Room.roomType);
        System.out.println("Room area      :"    + Room.roomArea);
		System.out.println("Room acmachine :"    + Room.roomAcmachine);

	}

}
