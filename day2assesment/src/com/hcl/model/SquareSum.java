package com.hcl.model;
//calculate sum of squares of even numbers

public class SquareSum {
	public static int SumOfSquaresOfEvenDigits(int number) {
		int n1;
		int sum = 0;

		while (number > 0) {
			n1 = number % 10;
			if ((n1 % 2) == 0)
				sum = n1 * n1;
			number /= 10;

		}
		return sum;

	}

}
