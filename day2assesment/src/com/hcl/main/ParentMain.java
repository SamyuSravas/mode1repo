package com.hcl.main;
import com.hcl.model.Child1;
import com.hcl.model.Child2;
import com.hcl.model.Parent;
public class ParentMain {
	private static final Parent C2 = null;

	public static void main(String[] args) {
		Parent P = new Parent();
		Child1 c1 = new Child1();
		Child2 c2 = new Child2();
		System.out.println(c1 instanceof Parent);
		System.out.println(C2 instanceof Parent);
		System.out.println(P instanceof Child1);
		System.out.println(P instanceof Child2);
		P = c1;
		System.out.println(P instanceof Child1);
		System.out.println(P instanceof Child2);
		P = c2;
		System.out.println(P instanceof Child1);
		System.out.println(P instanceof Child2);
	}
	}

}
}