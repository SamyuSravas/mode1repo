import java.util.Scanner;

public class Middle {
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		System.out.println("Enter a string:");
		String str = in.nextLine();
		System.out.println("the middle character in the string:" + Middle(str)+"\n");
		
	}

	public static  String Middle(String str)
	{
		int position;
		int length;
		if(str.length() % 2 ==0)
		{
			position = str.length() / 2 -1;
		    length = 2;
	}
	else
	{
	    position = str.length() /2;
		length = 1;
	}
	return str.substring(position, position + length);
}
}
	