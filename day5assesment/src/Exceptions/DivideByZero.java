package Exceptions;
import java.util.Scanner;
public class DivideByZero {

	    public static int quotient(int num1, int num2) throws ArithmeticException {
	        return num1 / num2;
	        
	    }
	    public static void main(String[] args){
	    Scanner sc = new Scanner(System.in);
	    int num1 = sc.nextInt();
	    int num2 = sc.nextInt();
	    int result = quotient(num1, num2);
	    System.out.printf("%d/%d=%d\n", num1, num2,result);
	    }

}
