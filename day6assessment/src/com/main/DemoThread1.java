package com.main;

public class DemoThread1 implements Runnable {

	public static void main(String[] args) {
		DemoThread1 obj1 = new DemoThread1();
		DemoThread1 obj3= new DemoThread1();
		obj1.run();
		

	}
	
	public DemoThread1() {

		Question1 t1 = new Question1();

		System.out.println("Name of t1:" + t1.getName());

		t1.start();

	}

	@Override
	public void run() {
		System.out.println("running child Thread in loop:");
		int counter = 1;
		while (counter < 11) {
			System.out.println(counter);
		counter = counter + 1;
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		}
	}

}


